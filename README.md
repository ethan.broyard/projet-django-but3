# Projet-Django-BUT3

## Énnoncé

Ce projet Django consiste à gérer une collection de Films, Séries ou Jeux Vidéos incluant leur année de
sortie, leur titre, réalisateur (ou Studio) et dʼautres informations de votre choix

**Nous avons choisi la collection de Jeux Vidéos**

## Lancement

- se déplacer dans :  `cd ./projet-django-but3/JeuxVideos/`

### BD

Créer la Base de Données vide

- `python3 ./manage.py makemigrations`
- `python3 ./manage.py sqlmigrate lesJeux/ 0013`
- `python3 ./manage.py migrate`

### Site

Lancer le site

- `python3 ./manage.py runserver`

Puis aller à l'url:

[http://127.0.0.1:8000/lesJeux/jeu](http://127.0.0.1:8000/lesJeux/jeu)


## Etapes réalisées
- Etape 0 : (une seule table pour commencer)
- Etape 1 : mise en place d'une relation 1-N
- Etape 2: mise en place d'une relation N-N

## Fonctionnalitées implémentées
- Tests
- Bootstrap
- CRUD pour toutes les tables

## Membres 

Ethan Broyard <br>
Vincent Bernardi
from django.db import models
from datetime import datetime, timedelta, date
from django.utils.html import format_html
from django.utils import formats
from django.urls import reverse
from django.core.exceptions import ValidationError
from fuzzywuzzy.fuzz import ratio

def get_partie_commune(nom1, nom2):
    # ex:   nom1 => star wars 2 ;  nom2 => star wars le retour
    #       donne => star wars
    partie_commune = ""
    minimum = min(nom1, nom2)
    for i in range(len(minimum)):
        if nom1[i] == nom2[i]:
            partie_commune += minimum[i]
    return partie_commune

def get_ratio_lettres_nom(nom1, nom2):
    # donne le ratio (en %) de lettres identiques entre nom1 et nom2
    return ratio(nom1, nom2)


def validate_date_inferieur_demain(value):
    if value > date.today():
        raise ValidationError(
            ("Les dates dans le futur ne sont pas permises"),
            params={"value": value},
        )
    
def validate_jeu_nom_presque_unique(nom): 
    # faute d'orthographe star wars <> star wors
    nom_existant = Jeu.objects.filter(nom__istartswith=nom.split(" ")[0])# on prend le premier mot

    if str(len(nom_existant)+1) in nom:
        print(nom)
        print("POUET")
    elif nom_existant.exists():
        raise ValidationError(
            # nom_existant est un QuerySet
            f"Le Jeu '{nom_existant[len(nom_existant)-1]}' existe. Essayer avec '{get_partie_commune(nom, nom_existant[0].get_nom())} {len(nom_existant)+1}'",
            params={"value": nom},
        )
    elif get_ratio_lettres_nom(nom, nom_existant[0].get_nom()) > 75: # 75 % , ~ 2 lettres
        raise ValidationError(
            # nom_existant est un QuerySet
            f"Avez vous essayé d'écrire '{nom_existant[len(nom_existant)-1]}' ? Sinon ré-appuyer sur 'Submit   '",
            params={"value": nom},
        )
    
        
class Personnage(models.Model):
    nom = models.CharField(max_length=250, unique=True)
    age = models.CharField(max_length=250, null=True)
    
    def __str__(self):
        return self.nom
    
    def get_absolute_url(self):
        return reverse('lesJeux:personnage-detail', kwargs={'pk': self.pk})

    # Getters
    def get_nom(self):
        return self.nom

    def get_age(self):
        return self.age
    
  
    # Setters
    def set_name(self, nom):
        self.nom = nom
        self.save()

    def set_age(self, age):
        self.age = age
        self.save()
    
  
class Editeur(models.Model):
    nom = models.CharField(max_length=250, unique=True)
    date_creation = models.DateField(null=True,  validators=[validate_date_inferieur_demain])
    logo = models.URLField()
   
    def __str__(self):
        return self.nom
    
    def get_absolute_url(self):
        return reverse('lesJeux:editeur-detail', kwargs={'pk': self.pk})

    # Getters
    def get_nom(self):
        return self.nom
    
    def get_date_creation(self):
        return self.date_creation

    def get_logo(self):
        return self.logo
    
    # Setters
    def set_name(self, nom):
        self.nom = nom
        self.save()
    
    def set_date_creation(self, date_creation):
        self.date_creation = date_creation
        self.save()

    def set_logo(self, logo):
        self.editlogoeur = logo
        self.save()


class Jeu(models.Model):
    nom = models.CharField(max_length=250, unique=True, validators=[validate_jeu_nom_presque_unique])
    description = models.TextField()
    genre = models.CharField(max_length=250)
    prix = models.IntegerField()
    editeur = models.ForeignKey(Editeur, on_delete=models.CASCADE, related_name="editeur", null=True)
    date_creation = models.DateField(null=True, validators=[validate_date_inferieur_demain])
    personnages = models.ManyToManyField(Personnage, related_name="personnages", null=True, blank=True)
    logo = models.URLField(null=True)
    url_steam = models.URLField(null=True)

    def __str__(self):
        return self.nom
    
    def get_absolute_url(self):
        return reverse('lesJeux:jeu-detail', kwargs={'pk': self.pk})

    # Getters
    def get_nom(self):
        return self.nom

    def get_description(self):
        return self.description
    
    def get_genre(self):
        return self.genre
    
    def get_prix(self):
        return self.prix
    
    def get_editeur(self):
        return self.editeur
    
    def get_date_creation(self):
        return self.date_creation
    
    def get_personnages(self):
         return self.personnages
    
    # Setters
    def set_name(self, nom):
        self.nom = nom
        self.save()

    def set_description(self, description):
        self.description = description
        self.save()

    def set_genre(self, genre):
        self.genre = genre
        self.save()

    def set_prix(self, prix):
        self.prix = prix
        self.save()

    def set_editeur(self, editeur):
        self.editeur = editeur
        self.save()

    def set_personnages(self, personnages):
         self.personnages = personnages
         self.save()

    def set_date_creation(self, date_creation):
        self.date_creation = date_creation
        self.save()


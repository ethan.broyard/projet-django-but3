from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import DetailView, ListView, CreateView, DeleteView, UpdateView
from .models import Jeu,Editeur, Personnage
from django.urls import reverse_lazy
from django.forms import DateInput, CharField,ModelMultipleChoiceField,CheckboxSelectMultiple
from django import forms
from django.contrib.admin.widgets import AdminDateWidget
from django.db import models
from django.urls import reverse


class JeuForm(forms.ModelForm):
    personnages = forms.ModelMultipleChoiceField(
            queryset=Personnage.objects.all(),
            widget=forms.CheckboxSelectMultiple,
            required=False
        )
    nom = forms.CharField()
    description = forms.CharField()
    genre = forms.CharField()
    prix = forms.CharField()
     
    class Meta:
        model= Jeu
        fields = ['nom', 'description', 'genre', 'prix','editeur', 'date_creation','personnages','logo','url_steam']
        
        
        

class JeuDetailView(DetailView):
    model = Jeu

class JeuList(ListView):
    model = Jeu

class JeuCreate(CreateView):
    model = Jeu
    form_class= JeuForm
    template_name= 'lesJeux/jeu_form.html'
     
    def get_form(self, form_class=None):
        form = super(JeuCreate, self).get_form(form_class)
        form.fields['date_creation'].widget = AdminDateWidget(attrs={'type': 'date'})
        return form

class JeuDelete(DeleteView):
    model = Jeu
    success_url = reverse_lazy('lesJeux:jeu_list')

class JeuUpdate(UpdateView):
    model = Jeu
    form_class= JeuForm
    template_name_suffix = '_update_form'

    def get_form(self, form_class=None):
        form = super(JeuUpdate, self).get_form(form_class)
        form.fields['date_creation'].widget = AdminDateWidget(attrs={'type': 'date'})
        return form

class EditeurDetailView(DetailView):
    model = Editeur

class EditeurList(ListView):
    model = Editeur

class EditeurCreate(CreateView):
    model = Editeur
    fields = ['nom', 'date_creation', 'logo']
   
    def get_form(self, form_class=None):
        form = super(EditeurCreate, self).get_form(form_class)
        form.fields['date_creation'].widget = AdminDateWidget(attrs={'type': 'date'})
        return form

class EditeurDelete(DeleteView):
    model = Editeur
    success_url = reverse_lazy('lesJeux:editeur_list')

class EditeurUpdate(UpdateView):
    model = Editeur
    fields = ['nom', 'date_creation', 'logo']
    template_name_suffix = '_update_form'

    def get_form(self, form_class=None):
        form = super(EditeurUpdate, self).get_form(form_class)
        form.fields['date_creation'].widget = AdminDateWidget(attrs={'type': 'date','placeholder':form.fields['date_creation']})
        return form
class PersonnageDetailView(DetailView):
    model = Personnage
    
class PersonnageList(ListView):
    model = Personnage

class PersonnageCreate(CreateView):
    model = Personnage
    fields = ['nom','age']
    
    
    # def get_form(self, form_class=None):
    #     formu= super(PersonnageCreate, self).get_form(form_class)
        
    #     CHOICES = FormsTools.JeuxToTuples(Jeu.objects.all())
        
    #     formu.fields['jeux'].widget = forms.ChoiceField(forms.MultipleChoiceField( required=True, widget=forms.CheckboxSelectMultiple(), choices=CHOICES),)
    #     return formu
   
class PersonnageDelete(DeleteView):
    model = Personnage
    success_url = reverse_lazy('lesJeux:personnage_list')

class PersonnageUpdate(UpdateView):
    model = Personnage
    fields = ['nom','age']
    template_name_suffix = '_update_form'

class PersonnageCreate2(CreateView):
    model = Personnage
    fields = ['nom','age']
    success_url = reverse_lazy('lesJeux:jeu_create')
from django.urls import path
from .views import *
from . import views
from django.views.generic import RedirectView

app_name = 'lesJeux' # Encapsule les urls de ce module dans le namespace musique
urlpatterns = [
    path('<int:pk>', JeuDetailView.as_view(), name='jeu-detail'),
    path('jeu', JeuList.as_view(), name='jeu_list'),
    path('jeu/create', JeuCreate.as_view(), name='jeu_create'),
    path('<int:pk>/delete/', JeuDelete.as_view(), name='jeu-delete'),
    path('jeu/<int:pk>', JeuUpdate.as_view(), name='jeu_update'),
    path('', JeuList.as_view(), name='jeu_list'),


    path('editeur/', EditeurList.as_view(), name='editeur_list'),
    path('editeur/<int:pk>', EditeurDetailView.as_view(), name='editeur-detail'),
    path('editeur/create', EditeurCreate.as_view(), name='editeur_create'),
    path('editeur/<int:pk>/delete/', EditeurDelete.as_view(), name='editeur-delete'),
    path('editeur/<int:pk>/update', EditeurUpdate.as_view(), name='editeur_update'),

    path('personnage/', PersonnageList.as_view(), name='personnage_list'),
    path('personnage/<int:pk>', PersonnageDetailView.as_view(), name='personnage-detail'),
    path('personnage/create', PersonnageCreate.as_view(), name='personnage_create'),
    path('personnage/<int:pk>/delete/', PersonnageDelete.as_view(), name='personnage-delete'),
    path('personnage/<int:pk>/update', PersonnageUpdate.as_view(), name='personnage_update'),
    path('jeu/create/personnage', PersonnageCreate2.as_view(),name='personnage_create2')
]

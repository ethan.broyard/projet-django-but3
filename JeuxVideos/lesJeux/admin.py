from django.contrib import admin
from lesJeux.models import Jeu,Personnage,Editeur

class JeuAdmin(admin.ModelAdmin):
    class JeuAdmin(admin.ModelAdmin):
        list_display = ('nom', 'description','genre','prix', 'date_creation')
        read_only=('date_creation')

    class PersonnageAdmin(admin.ModelAdmin):
        list_display = ('nom', 'age')
      

    class EditeurAdmin(admin.ModelAdmin):
        list_display = ('nom','logo', 'date_creation')
        read_only=('date_creation')

admin.site.register(Jeu,JeuAdmin)
admin.site.register(Personnage,JeuAdmin)
admin.site.register(Editeur,JeuAdmin)


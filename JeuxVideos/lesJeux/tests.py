from django.urls import reverse
from django.test import TestCase
from django.urls.exceptions import NoReverseMatch
from datetime import datetime, date

from .models import Jeu, Editeur, Personnage        

class JeuTestCase(TestCase):
    def setUp(self):
        edit = Editeur.objects.create(nom='Edy', date_creation=date(2024, 6, 5), logo='un cercle vert')
        jeu = Jeu.objects.create(nom='je', description='desc', genre='action', prix=25, editeur=edit, date_creation=date(2012, 5, 3))

    def test_jeu_url_name(self):
        try:
            url = reverse('lesJeux:jeu-detail', args=[1])
        except NoReverseMatch:
            assert False

    def test_jeu_url(self):
        jeu = Jeu.objects.get(nom='je')
        url = reverse('lesJeux:jeu-detail', args=[jeu.pk])
        response = self.client.get(url)
        assert response.status_code == 200
    

    def test_jeu_unique_name(self):
        edit = Editeur.objects.get(nom='Edy')
        try:
            Jeu.objects.create(nom='je', description='une description', genre='rythme', prix=0 , editeur=edit, date_creation=date(2003, 2, 1))
            assert False
        except:
            assert True

class EditeurTestCase(TestCase):
    def setUp(self):
        edit = Editeur.objects.create(nom='Edy', date_creation=date(2024, 6, 5), logo='un cercle vert')

    def test_editeur_url_name(self):
        try:
            url = reverse('lesJeux:editeur-detail', args=[1])
        except NoReverseMatch:
            assert False

    def test_editeur_url(self):
        editeur = Editeur.objects.get(nom='Edy')
        url = reverse('lesJeux:editeur-detail', args=[editeur.pk])
        response = self.client.get(url)
        assert response.status_code == 200
    
    def test_editeur_unique_name(self):
        try:
            edit2 = Editeur.objects.create(nom='Edy', date_creation=date(1998, 5, 3), logo='une fenetre de quatres couleurs')
            assert False
        except:
            assert True


class PersonnageTestCase(TestCase):
    def setUp(self):
        edit = Editeur.objects.create(nom='Edy', date_creation=date(2024, 6, 5), logo='un cercle vert')
        jeu = Jeu.objects.create(nom='je', description='desc', genre='action', prix=25 , editeur=edit, date_creation=date(2012, 5, 3))
        jeu.personnages.create(nom="mario", age="34")

    def test_personnage_url_name(self):
        try:
            url = reverse('lesJeux:personnage-detail', args=[1])
        except NoReverseMatch:
            assert False

    def test_personnage_url(self):
        personnage = Personnage.objects.get(nom='mario')
        url = reverse('lesJeux:personnage-detail', args=[personnage.pk])
        response = self.client.get(url)
        assert response.status_code == 200
    
    def test_personnage_ajouter_dans_jeu(self):
        jeu = Jeu.objects.get(nom='je')
        try:
            jeu.personnages.create(nom="luigi", age="28")
            assert False
        except:
            assert True

    def test_personnage_unique_name(self):
        jeu = Jeu.objects.get(nom='je')
        try:
            jeu.personnages.create(nom="mario", age="45")
            assert False
        except:
            assert True
